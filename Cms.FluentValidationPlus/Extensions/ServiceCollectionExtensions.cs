using System.Globalization;
using Cms.FluentValidationPlus.Filters;
using Cms.FluentValidationPlus.Middlewares;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Cms.FluentValidationPlus.Extensions;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddFluentValidationLocalization(
        this IServiceCollection services,
        string resourcesPath,
        params string[] supportedCultures)
    {
        // Register middlewares and filters
        services.AddTransient<FluentValidationExceptionMiddleware>();
        services.AddMvc(options =>
        {
            options.Filters.Add<FluentValidationFilter>();
        });

        // Add localization
        services.AddLocalization(options => options.ResourcesPath = resourcesPath);

        // Configure supported cultures and localization options
        services.Configure<RequestLocalizationOptions>(options =>
        {
            var cultures = supportedCultures.Select(c => new CultureInfo(c)).ToList();

            options.DefaultRequestCulture = new RequestCulture(
                cultures.FirstOrDefault() ?? new CultureInfo("en-US"));
            // Default to the first culture provided or "en-US"
            options.SupportedCultures = cultures;
            options.SupportedUICultures = cultures;
            options.FallBackToParentCultures = false;
            options.FallBackToParentUICultures = false;
        });

        return services;
    }

    public static IApplicationBuilder UseCustomValidation(this IApplicationBuilder app)
    {
        // Get the localization options from the application services
        var locOptions = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
        app.UseRequestLocalization(locOptions.Value);

        return app;
    }
}
