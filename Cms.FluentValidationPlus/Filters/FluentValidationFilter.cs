using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Cms.FluentValidationPlus.Filters;

public class FluentValidationFilter : IActionFilter
{
    // Method executed before the action in the controller
    public void OnActionExecuting(ActionExecutingContext context)
    {
        const int statusCodeBadRequest = 400;

        // Check if the received model is valid
        if (!context.ModelState.IsValid)
        {
            // Collect model validation errors
            var errors = context.ModelState.Values
                .SelectMany(v => v.Errors)
                .Select(e => new { message = e.ErrorMessage })
                .ToList();

            // Add validation errors to the HTTP context
            context.HttpContext.Items["ValidationErrors"] = errors;

            // Set the BadRequest error response with details of validation errors
            context.Result = new BadRequestObjectResult(new
            {
                message = "Validation failed",
                code = statusCodeBadRequest,
                errors
            })
            {
                StatusCode = statusCodeBadRequest,
                ContentTypes = { "application/json" }
            };
        }
    }

    // Method executed after the action in the controller
    public void OnActionExecuted(ActionExecutedContext context)
    {
        // Method intentionally left empty
    }
}
