using System.Text.Json;
using FluentValidation;
using Microsoft.AspNetCore.Http;

namespace Cms.FluentValidationPlus.Middlewares;

public class FluentValidationExceptionMiddleware(RequestDelegate next)
{
    private readonly RequestDelegate _next = next;

    public async Task InvokeAsync(HttpContext context)
    {
        // Validate if HttpContext is not null
        ValidateParameters(context);

        // Handle the request asynchronously
        await HandleRequestAsync(context);
    }

    private static void ValidateParameters(HttpContext context)
        => ArgumentNullException.ThrowIfNull(context);

    private async Task HandleRequestAsync(HttpContext context)
    {
        const int statusCodeBadRequest = 400;

        try
        {
            // Attempt to execute the next middleware in the pipeline
            await _next(context);
        }
        catch (ValidationException ex)
        {
            // If a ValidationException is caught, handle it
            context.Response.StatusCode = statusCodeBadRequest;
            context.Response.ContentType = "application/json";

            // Extract error messages from the ValidationException
            var errors = ex.Errors.Select(e => new { message = e.ErrorMessage }).ToList();
            var response = new { code = statusCodeBadRequest, errors };

            // Serialize response to JSON
            var json = JsonSerializer.Serialize(response);
            await context.Response.WriteAsync(json);
        }
    }
}
